﻿using UnityEngine;
using System.Collections;

public class MoveFloor : MonoBehaviour {

    private Vector3 initialPosition;
    private Vector3 pastPosition;
    void Start () {
        initialPosition = transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        transform.position= new Vector3(Mathf.Sin(Time.time) * 5.0f + initialPosition.x, initialPosition.y, initialPosition.z);

    }

}

//プレイヤーが乗ったときにプレイヤーだけ置いて行かれてしまうのでコード追記
