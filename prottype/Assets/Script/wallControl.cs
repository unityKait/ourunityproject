﻿using UnityEngine;
using System.Collections;

public class wallControl : MonoBehaviour {
    public float periodicity;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Mathf.Sin(Mathf.Floor(Time.time * periodicity)) > 0)
        {
            GetComponent<BoxCollider>().enabled = true;
            GetComponent<MeshRenderer>().enabled = true;
        }
        else
        {
            GetComponent<BoxCollider>().enabled = false;
            GetComponent<MeshRenderer>().enabled = false;
        }

    }
}

