﻿using UnityEngine;
using System.Collections;

public class PlayerStatus : MonoBehaviour {
    public GameObject ragDoll;

    int hp;
    const int MAX_HP=100;

	// Use this for initialization
	void Start () {
        hp = MAX_HP;
	}

    void Update()
    {
        if (hp <= 0)
        {
            Debug.Log("お前はもう死んでいる");
            Instantiate(ragDoll, transform.position,transform.rotation);
            Destroy(gameObject);
        }
    }
	
	void damage(int dmg = 20)
    {
        hp -= dmg;
        Debug.Log("ダメージを受ける" + ":" + dmg + ":" + hp);
    }

    void death()
    {
        hp = 0;
        Debug.Log("あなたは死んだ");
    }
}
