﻿using UnityEngine;
using System.Collections;

public class ElevatorMove : MonoBehaviour {
    public float range;//1階と２階の距離
    public float speed;//エレベーターのスピード

    bool firstFloor;//１階かどうか
    bool moving;//動いてるかどうか
    float firstY;//計算用に動き始める位置を記録している
    float secondY;
    //Rigidbody myRigidbody;
    void Start () {
        firstFloor = true;
        moving = false;
        //myRigidbody = GetComponent<Rigidbody>();
        firstY = transform.position.y;
        secondY = firstY + range;
    }
	
	void Update () {
        if (moving)
        {
            if (firstFloor)MoveFloor(1);
            if (!firstFloor) MoveFloor(-1);
            FloorCheck();
        }
	}

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
           // Debug.Log("エレベーター起動");
            moving = true;
            //myRigidbody.constraints = RigidbodyConstraints.FreezeRotation| RigidbodyConstraints.FreezePositionX| RigidbodyConstraints.FreezePositionZ;
            //myRigidbody.velocity = new Vector3(0, speed, 0);
        }
    }

    void MoveFloor(int d)
    {
        //myRigidbody.AddForce(new Vector3(0, d * speed, 0));
        transform.Translate(new Vector3(0, d * speed*Time.deltaTime, 0));
    }

    void FloorCheck()
    {
        if (firstFloor&& secondY < transform.position.y)//1階から2階へ
        {
            //Debug.Log("2階です");
            transform.position = new Vector3(transform.position.x, secondY, transform.position.z);
            moving = false;
            firstFloor = false;
            //myRigidbody.constraints = RigidbodyConstraints.FreezeAll;
        }
        else if (!firstFloor && transform.position.y < firstY)//2階から1階へ
        {
            //Debug.Log("1階です");
            transform.position = new Vector3(transform.position.x, firstY, transform.position.z);
            moving = false;
            firstFloor = true;
            //myRigidbody.constraints = RigidbodyConstraints.FreezeAll;
        }
    }
}
