﻿using UnityEngine;
using System.Collections;

public class OneWayFloor : MonoBehaviour {
    public int pushPower;
	
	void OnTriggerStay(Collider col)
    {
        Vector3 vec=-transform.right;
        col.GetComponent<Rigidbody>().AddForce(vec * pushPower);
    }
}
