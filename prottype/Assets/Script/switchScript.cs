﻿using UnityEngine;
using System.Collections;

public class switchScript : MonoBehaviour {

    public GameObject door;
    private Collider doorCollider;
    private MeshRenderer doorRender;

    void Start()
    {
        doorCollider = door.GetComponent<BoxCollider>();
        doorRender = door.GetComponent<MeshRenderer>();
    }

	void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            doorCollider.enabled = false;
            doorRender.enabled = false;
        }
    }
}
