﻿using UnityEngine;
using System.Collections;

public class JumpTrigger : MonoBehaviour {
    public int jumpPower;
    public float shotAngle;
	
    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            Vector3 vec = new Vector3(jumpPower * Mathf.Cos(shotAngle), jumpPower * Mathf.Sin(shotAngle), 0);
            col.GetComponent<Rigidbody>().AddForce(vec);
        }
    }
}
